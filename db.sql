SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `main_database` (
  `email` varchar(64) NOT NULL,
  `lastname` varchar(64) NOT NULL,
  `firstname` varchar(64) NOT NULL,
  `phone` varchar(64) NOT NULL,
  `message` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `main_database` (`email`, `lastname`, `firstname`, `phone`, `message`, `time`) VALUES
('3demon2@bk.ru', 'Nefedov', 'Dmitrii', '000', 'testmessage, keep calm 2', '2016-08-04 19:02:58'),
('3demon@bk.ru', 'Nefedov', 'Dmitrii', '000', 'testmessage, keep calm', '2016-08-03 23:09:32'),
('test2@test.com', 'test', 'test', '01', 'hello world', '0000-00-00 00:00:00'),
('test3@test.com', 'test', 'test', '01', 'hello world, фпфшт', '0000-00-00 00:00:00'),
('test4@test.com', 'test', 'test', '01', 'hello world, фпфшт', '0000-00-00 00:00:00'),
('test@test.com', 'test', 'test', '01', 'hello world', '0000-00-00 00:00:00');
