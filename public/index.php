<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Naira Trade</title>
    <link href="https://fonts.googleapis.com/css?family=Actor" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>


<div id="main">
    <div id="first">
        <div class="toplane">
            <div class="logoback">
                <div class="logo"></div>
            </div>


            <div class="social">
                <a class="socicon" href="dbcontent.php" target="_blank" title="Facebook">
                    <img src="images/Facebook.png">
                </a>
                <a class="socicon" href="#" target="_blank" title="LinkedIn">
                    <img src="images/LinkedIn.png">
                </a>
                <a class="socicon" href="#" target="_blank" title="Twitter">
                    <img src="images/Twitter.png">
                </a>
                <a class="socicon" href="#" target="_blank" title="Instagram">
                    <img src="images/Instagram.png">
                </a>
                <a class="contact" href="#third" target="_blank" title="Contact Us">
                    Contact Us
                </a>
            </div>

            <div class="centertextblock">
                <h2>A DIFFERENT BUSINESS CONSULTANCY</h2>
                <h3>experience . data analytics . technology</h3>
            </div>
        </div>

        <div class="midlane">
            <img class="ico" src="images/exp.png">
            <img class="ico" src="images/data.png">
            <img class="ico" src="images/tech.png">

        </div>

        <div class="botlane">

            <a href="#second" id="smooth">
                <div id="next1">
                    <figure class="img">
                        <figcaption>Learn more</figcaption>
                        <img alt="Learn more" src="images/Down-Arrow.png">
                    </figure>
                </div>
            </a>

        </div>
    </div>

    <div id="second">

        <div class="midcontainer">
            <img src="images/cont1.png" class="contimg"/>
            <img src="images/right-arrow.png" class="contarrow"/>
            <span class="conttext">We develop solutions and services for businesses in developing countries to solve the challenges they face in funding and supply management.</span>
            <img src="images/cont2.png" class="contimg"/>
            <img src="images/right-arrow.png" class="contarrow"/>
            <span class="conttext">Using data driven insights and the right application of technology,</span>
            <img src="images/cont3.png" class="contimg"/>
            <img src="images/right-arrow.png" class="contarrow"/>
            <span class="conttext">while leveraging our experience and knowledge in the commerce of developing</span>

        </div>

        <a href="#third" id="smooth">
            <div id="next2">
                <figure class="img">
                    <figcaption>How we can help</figcaption>
                    <img alt="How we can help" src="images/Down-Arrow-copy.png">
                </figure>
            </div>
        </a>
    </div>

    <div id="third">
        <div class="about">
            <div>
                <img src="images/net-icon.png" class="aboutico">
                <div class="abouttext">
                    <span>See how we are helping individuals and communities access capital through the power of their social
                    network</span>
                </div>
            </div>
            <div class="line"></div>
            <div class="knabu"></div>

            <div>
                <img src="images/fiber-icon.png" class="aboutico">
                <div class="abouttext">
                    <span>Offering homes in housing estates with high speed fiber optic internet access installed and managed within the housing estate</span>
                </div>
            </div>
            <div class="line"></div>
            <div class="fiber"></div>

        </div>

        <div class="inputform">

            <form id="formx" action="javascript:void(null);" onsubmit="call()" method="post">
                <div class="whitetext">Contact Us</div>
                <input name="lastname" class="monostring" size="50" required="required" type=text placeholder="Last Name"/>
                <input name="firstname" class="monostring" size="50" required="required" type=text placeholder="First Name"/>
                <input name="phone" class="monostring" size="50" required="required" type=tel placeholder="Telephone No"/>
                <input name="email" class="monostring" size="50" required="required" type=email placeholder="Email"/>
                <textarea name="message" class="polystring" rows="8" cols="50" required="required" placeholder="Message"></textarea>

                <input class="button" type=submit value="Send"/>

            </form>


            <div class="address">
                <div class="big">
                    <div>

                        <br>
                        <br>
                        <img src="images/mail-icon.png" class="addressico">
                        info@nairatrade.com
                        <img src="images/phone-icon.png" class="addressico">
                        +123 123 123 4567
                    </div>

                    <div>
                        Plot 14a Block 53<br>
                        Adebayo Doherty Street,<br>
                        Off Admiralty Way,<br>
                        Lekki Phase1,<br>
                        Lagos, Nigeria<br>
                        <br>
                        Company Registration #: RC 826910
                    </div>
                    <div class="social2">
                        <a class="socicon" href="#" target="_blank" title="Facebook">
                            <img src="images/Facebook.png">
                        </a>
                        <a class="socicon" href="#" target="_blank" title="LinkedIn">
                            <img src="images/LinkedIn.png">
                        </a>
                        <a class="socicon" href="#" target="_blank" title="Twitter">
                            <img src="images/Twitter.png">
                        </a>
                        <a class="socicon" href="#" target="_blank" title="Instagram">
                            <img src="images/Instagram.png">
                        </a>
                    </div>
                </div>

                <div class="small">
                    <br>
                    <br>
                    <img src="images/mail_ico_dark.png" class="addressico">
                    info@nairatrade.com<br>
                    <img src="images/phone_ico_dark.png" class="addressico">
                    +123 123 123 4567
                    <div class="social2">
                        <a class="socicon" href="#" target="_blank" title="Facebook">
                            <img src="images/Facebook.png">
                        </a>
                        <a class="socicon" href="#" target="_blank" title="LinkedIn">
                            <img src="images/LinkedIn.png">
                        </a>
                        <a class="socicon" href="#" target="_blank" title="Twitter">
                            <img src="images/Twitter.png">
                        </a>
                        <a class="socicon" href="#" target="_blank" title="Instagram">
                            <img src="images/Instagram.png">
                        </a>
                    </div>
                    <div>
                        Plot 14a Block 53<br>
                        Adebayo Doherty Street,<br>
                        Off Admiralty Way,<br>
                        Lekki Phase1,<br>
                        Lagos, Nigeria<br>
                        <br>
                        Company Registration #: RC 826910
                    </div>

                </div>
            </div>
        </div>
        <div class="copyright">copyright © Naira Trade 2016</div>
    </div>
</div>

<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="js/main.js"></script>
<script src="js/smoothScroll.js"></script>
<script type="text/javascript" language="javascript">
    function call() {
        var msg = $('#formx').serialize();
        $.ajax({
            type: 'POST',
            url: 'handler.php',
            data: msg,
            success: function (data) {
//                $('#results').html(data);
                alert('successfully sended');
            },
            error: function (xhr, str) {
                alert('Возникла ошибка: ' + xhr.responseCode);
            }
        });

    }
</script>

</body>
</html>
