<?php
require_once __DIR__ . '/../connection.php'; // подключаем скрипт

// подключаемся к серверу
$link = mysqli_connect($host, $user, $password, $database);
if (!$link) {
    die('Ошибка ' . mysqli_connect_error());
}

// выполняем операции с базой данных
$sql = 'SELECT * FROM `main_database` ORDER BY `time` ASC';
$result = mysqli_query($link, $sql) or die('Ошибка ' . mysqli_error($link));
if ($result) {

    $rows = mysqli_num_rows($result); // количество полученных строк
    echo '<table>';
    echo "<tr><td>email</td><td>lastname</td><td>firstname</td><td>phone</td><td>message</td><td>timestamp</td></tr>";
    for ($i = 0; $i < $rows; ++$i) {
        $row = mysqli_fetch_row($result);
       echo "<tr><td>$row[0]</td><td>$row[1]</td><td>$row[2]</td><td>$row[3]</td><td>$row[4]</td><td>$row[5]</td></tr>";


    }
    echo '<table>';
    // очищаем результат
    mysqli_free_result($result);
}
// закрываем подключение
mysqli_close($link);
?>